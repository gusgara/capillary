function[fL,fR] = DOFDM_mod(u,m,f)

for i=1:m-2
    fR(i) = f(u(i+1));
    fL(i) = f(u(i));
end

end
%This function verify the erros and convergence rate
function verification(unew,uanal,cvol,dxsv,keywrite,ucv)
%User mesage
disp('---------------------------------------');
disp('>> Ploting error analisys!');
np = length(ucv);    
%Calculate the absolute error
errorinf = norm(uanal - unew,inf)
%Calculate the relative error of "el2" ("relerrorL2")
%errorl1 = norm((uanal - unew).*mean(cvol),1)
errorl1 = norm((uanal - unew)/np,1)

disp('---------------------------------------');
disp('>> Ploting error (in log10 scale)!');

errorinflog = log10(errorinf)
errorl1log = log10(errorl1)

%--------------------------------------------------------------------------
%Write and Plot error file

%Open the file "error.dat" with its respective path
%This file can receive either a first value or an accumulate value:
%To write for first time the file, "keywrite" must receive "i" (initial)
if strcmp(keywrite,'i') == 1
    %erroreval = fopen('C:\\users\\gustavo\\Dropbox\\prototipo-prueba\\sin x g-l\\errorevalk3.dat','w');
    erroreval = fopen('/home/ggr/Dropbox/prototipo-prueba/sin_x_g-l/errorevalk6.dat','w');
    %Print the error value
    fprintf(erroreval,'%f \t%f \t%f\r\n',...
        log10(dxsv),log10(errorinf),log10(errorl1));

    %Read the file "erroreval.dat"
%     errormatrix = ...
%         textread('C:\\users\\gustavo\\Dropbox\\prototipo-prueba\\sin x g-l\\erroreval.dat',...
%         '','delimiter',' ');

     errormatrix = ...
        textread('/home/ggr/Dropbox/prototipo-prueba/sin_x_g-l/errorevalk6.dat',...
        '','delimiter',' ');
%To write an accumulate time in the file, "keywrite" must receive "a" 
%(accumulated)
elseif strcmp(keywrite,'a') == 1
    %erroreval = fopen('C:\\users\\gustavo\\Dropbox\\prototipo-prueba\\sin x g-l\\erroreval.dat','a');
    erroreval = fopen('/home/ggr/Dropbox/prototipo-prueba/sin_x_g-l/errorevalk6.dat','a');
    %Print the error value
    fprintf(erroreval,'%f \t%f \t%f\r\n',...
        log10(dxsv),log10(errorinf),log10(errorl1));

    %Read the file "erroreval.dat"
%     errormatrix = ...
%         textread('C:\\users\\gustavo\\Dropbox\\prototipo-prueba\\sin x g-l\\erroreval.dat',...
%         '','delimiter',' ');
    errormatrix = ...
        textread('/home/ggr/Dropbox/prototipo-prueba/sin_x_g-l/errorevalk6.dat',...
        '','delimiter',' ');
        
    %------------------------------------------------------------------
    %Convergence RATE
        
    %User mesage
    disp('---------------------------------------');
    disp('>> Convergence Rate (inf and L1 norms):');
    %If there is accumulated erros analisys, we can calculate the
    %convergence rate, R.
    %To max norm:
    Rinf = ...
        abs((errormatrix(size(errormatrix,1),2) - errormatrix(size(errormatrix,1) - 1,2))/...
        (errormatrix(size(errormatrix,1),1) - errormatrix(size(errormatrix,1) - 1,1)))
    %To L1 norm:
    RL1 = ...
        abs((errormatrix(size(errormatrix,1),3) - errormatrix(size(errormatrix,1) - 1,3))/...
        (errormatrix(size(errormatrix,1),1) - errormatrix(size(errormatrix,1) - 1,1)))
end  %End of IF
    
%--------------------------------------------------------------------------
%GRAPHICS

%Plot the error field
plot(errormatrix(:,1),errormatrix(:,2),'-ro');
hold on;
plot(errormatrix(:,1),errormatrix(:,3),'-bs');
%plot(errormatrix(:,1),errormatrix(:,4),'-kv');
hold off;
axis square
grid on;
xlabel('Log10(dx)');
ylabel('Log10(error)');
title('Spectral Finite Volume (Gauss-Lobatto Spliting)');
legend('Norm Inf','Norm L1');


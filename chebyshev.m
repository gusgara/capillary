
function [res] = chebyshev(pontos, a, b)

%mu = 0.6;
 
%Pontos de Chebyshev
%x = (b+a-(b-a)*cos((2*(0:pontos-1)+1)*pi/(2*pontos)))/2;
%Pontos de Gauss-Lobatto
x = (b+a-(b-a)*cos(((0:pontos-1)+1)*pi/(pontos+1)))/2;
%Pontos hyperbolic tangent
%x = (b+a+(b-a)*tanh(((0:pontos-1)+1)*(2*mu/(pontos + 1)) - mu )/tanh(mu))/2;

res = [a x b];

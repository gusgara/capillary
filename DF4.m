%UNIVERSIDADE FEDERAL DE PERNAMBUCO
%CENTRO DE TECNOLOGIA E GEOCIENCIAS
%PROGRAMA DE POS GRADUACAO EM ENGENHARIA MEC�NICA
%TOPICOS ESPECIAIS EM DINAMICA DOS FLUIDOS COMPUTACIONAL
%--------------------------------------------------------------------------
%Subject: numerical routine to solve "FDM" 1D
%Type of file: MAIN
%Criate date: 6/10/2014
%Modify data: 27/10/2014
%Programer: Gustavo Galindez Ramirez
%--------------------------------------------------------------------------
%Goals:
%To Determinate a saturation field to 1D transient problem for  two phase flow
%with capillary pressure
%%
%Clear memory and scream
clc;
clear all;
close all;
%Out of data
%Tipo de formato

%Number of cells
M =512;
%Courant number
CFL = 0.01;
%Total time
finaltime = 10000.0;
%Domain
L = 0.03;
%Mesh data:
h = L/M;
%%
% gam = 1; %Unidirecional flow R = 1
% gam = 0; %Bidireccional flow R = 0
gam = 'B_L'; % Boundary wetting flux/total fluid flux
%% Model parameters two flow phase

fi = 0.2; %Porosidade (acumulacao de fluidos no meio poroso)
Ko = 1*10^-10;%(m^2)%Permeabilidade absoluta
A = 3*10^-7;
%qt = 1*10^(-3);%The velocity at which a fixed saturation Sw travels through the porous medium.(m/s)

model = 1;
switch model
    case 1
        %% Modelo de Corey and Burdine (1953)
        %Residual saturation
        Sor= 0.0001;
        Swr= 0.0001;
        %Viscosity
        mio = 1;
        miw = 1;
        
        lambda = 2;
        po = 5000;
        krw = @(s) ((s-Swr)./(1-Swr)).^(3+(2./lambda));%Relative permeability of the wetting  phase
        krn = @(s) (1-((s-Swr)./(1-Swr))).^2.*(1-((s-Swr)./(1-Swr)).^(1+(2./lambda)));%Relative permeability non-wetting phase
        PcD = @(s) po.*((s-Swr)./(1-Swr)).^(-1./lambda);
        dPcDdSw =  @(s) -po./(lambda.*((s-Swr)./(1-Swr)).^((1 + lambda)./lambda));
        f = @(s) (1 + (miw./mio).*((1-((s-Swr)./(1-Swr))).^2.*(1-((s-Swr)./(1-Swr)).^(1+(2./lambda))))./(((s-Swr)./(1-Swr)).^(3+(2./lambda)))).^-1;
        df = @(s) ((miw.*(2.*((s-Swr)./(1-Swr)) - 2).*(((s-Swr)./(1-Swr)).^(2./lambda + 1) - 1))./(mio.*((s-Swr)./(1-Swr)).^(2./lambda + 3)) - (miw.*(2./lambda + 3).*(((s-Swr)./(1-Swr)).^(2./lambda + 1) - 1).*(((s-Swr)./(1-Swr)) - 1).^2)./(mio.*((s-Swr)./(1-Swr)).^(2./lambda + 4)) + (miw.*((s-Swr)./(1-Swr)).^(2./lambda).*(2./lambda + 1).*(((s-Swr)./(1-Swr)) - 1).^2)./(mio.*((s-Swr)./(1-Swr)).^(2./lambda + 3)))./((miw.*(((s-Swr)./(1-Swr)).^(2./lambda + 1) - 1).*(((s-Swr)./(1-Swr)) - 1).^2)./(mio.*((s-Swr)./(1-Swr)).^(2./lambda + 3)) - 1).^2;
        D = @(s) -(Ko./mio).*(1-((s-Swr)./(1-Swr))).^2.*(1-((s-Swr)./(1-Swr)).^(1+(2./lambda)))...
            .*(1 + (miw./mio).*((1-((s-Swr)./(1-Swr))).^2.*(1-((s-Swr)./(1-Swr)).^(1+(2./lambda))))./(((s-Swr)./(1-Swr)).^(3+(2./lambda)))).^-1 ...
            .*  -po./(lambda.*(s).^((1 + lambda)./lambda));
        
    case 2
        % Van Genuchten and Parker model (1987)
        m = 0.5;
        po = 1000;
        %Viscosities
        mio = 0.001;
        miw = 0.050;
        %Residual saturation
        Sor= 10^-4;
        Swr= 10^-4;
        
        Pc = @(s) po.*((s.^(-1./m))-1).^(1-m);
        krw = @(s) (((s- Swr)./(1-Sor)).^(0.5)).*(1-(1-(((s- Swr)./(1-Sor)).^(1./m))).^m).^2;
        kro = @(s) ((1-((s- Swr)./(1-Sor))).^(0.5)).*(1-((s- Swr)./(1-Sor)).^(1/m)).^(2.*m);
        f = @(s) 1./(1+(miw./mio).*((((1-((s- Swr)./(1-Sor))).^(0.5)).*(1-((s- Swr)./(1-Sor)).^(1/m)).^(2.*m))./((((s- Swr)./(1-Sor)).^(0.5)).*(1-(1-(((s- Swr)./(1-Sor)).^(1./m))).^m).^2)));
        D = @(s) -(Ko./mio).*((1-((s- Swr)./(1-Sor))).^(0.5)).*(1-((s- Swr)./(1-Sor)).^(1/m)).^(2.*m)...
            .*(1./(1+(miw./mio).*((((1-((s- Swr)./(1-Sor))).^(0.5)).*(1-((s- Swr)./(1-Sor)).^(1/m)).^(2.*m))./((((s- Swr)./(1-Sor)).^(0.5)).*(1-(1-(((s- Swr)./(1-Sor)).^(1./m))).^m).^2))))...
            .*(((m-1).*po.*s.^((-1./m)-1).*(-1 + s.^(-1./m)).^(-m))./m);
        
end

% Function g(t)
func_gt = 0;
switch func_gt
    case 0
        gt = @(t) 1;
    case 1
        gt = @(t) (1./(1+t));
    case 3
        gt = @(t) t.^(-1./3);
    case 4
        gt = @(t) exp(sin(t./100));
    case 5
        gt = @(t) (1+sin(t./10))./(1+abs(sin(t./10)));
end

%% 		Definition of grid numbering
%       x=0    					                 x=L
% grid   |---o---|---o---|---o---  ...  --|---o---|
%            1   1   2   2   3           M-1  M
%% Initial condition
x = h*(1:M) - h/2;

%Initial solution
u_an = zeros(1,M);
for i=2:M-1
    u_an(i) = Swr;
end
%%
Sw = Swr:0.01:1-Sor;%vetor condicao inicial para avaliar a df/du

%dfdu = df(s);

Dmax = max(D(Sw));

%% Calculo do delta_t
%Velocity(max)
%amax = max(abs(dfdu));

dt = 0.5;

Stab_Cond = (Dmax/fi)*(dt/(h^2))

pause(0.5)

%dt = CFL*h/amax;

NSTEP = round(finaltime/dt);

ploter= [1 NSTEP/4 NSTEP/2 3*NSTEP/4 NSTEP];
%% Start a stopwatch timer.
tic;
tn = 0;
%%gif
% figure(1)
% filename = 'test.gif';
for n= 1:NSTEP;
    
    %NUMERICAL SOLUTION
    tnp = tn + dt;   % = t_{n+1}
    
    qt = A.*gt(tnp); %Volumetric flux [m/s]
    
    %Boundary conditions
    u_an(1) = 1-Sor;
    u_an(end) = Swr;
    
    u0= u_an;
    for istage= 1:3;
        switch gam
            
            case 0 %Bidirectional
                
                Ri = 0;
                
                %Capillary term
                %D+1/2 and D-1/2
                for j=1:M-2
                    
                    DR = D((u_an(j+2) + u_an(j+1))/2);%D+1/2
                    DL = D((u_an(j+1) + u_an(j))/2);%D-1/2
                    
                    fDR = DR*(u_an(j+2) - u_an(j+1));
                    fDL = DL*(u_an(j+1) - u_an(j));
                    
                    Rd (j)= (1/(fi*(h^2)))*(fDR-fDL);
                    
                end
                
            case 1 % Unidirectional
                %Advective term
                
                [fL,fR]= DOFDM_mod(u_an,M,f);
                
                Ri= -(qt./(fi.*h)).*(fR-fL);
                
                %Capillary term
                %D+1/2 and D-1/2
                for j=1:M-2
                    
                    DR = D((u_an(j+2) + u_an(j+1))/2);%D+1/2
                    DL = D((u_an(j+1) + u_an(j))/2);%D-1/2
                    
                    fDR = DR*(u_an(j+2) - u_an(j+1));
                    fDL = DL*(u_an(j+1) - u_an(j));
                    
                    Rd (j)= (1/(fi*(h^2)))*(fDR-fDL);
                    
                end
            case 'B_L'%Buckley Leverett
                
                %Advective term
                
                [fL,fR]= DOFDM_mod(u_an,M,f);
                
                Ri= -(qt./(fi.*h)).*(fR-fL);
                
                Rd = 0;
        end
        
        
        
        
        %Residual
        %Rd = 0;
        R= [0 (Ri+Rd) 0];
        
        if istage==1;
            u1= u0 + dt*R;
            u_an= u1;
        elseif istage==2;
            u2= (3/4)*u0 + (1/4)*u1 + (1/4)*dt*R;
            u_an= u2;
        else;
            u_an= (1/3)*u0 + (2/3)*u2 + (2/3)*dt*R;
        end;
    end;
    
    
    if ismember(n,ploter)==1;
        figure(1);plot(x,u_an,'k','marker','o','markersize',4);
        %axis ([0 L -0.2 (1-Sor)]);
        hold on;grid on;drawnow
        
    end;
    
    % Animated gif file
    %    if ismember(n,ploter) == 1
    %       frame = getframe(1);
    %       im = frame2im(frame);
    %       [imind,cm] = rgb2ind(im,256);
    %       %% Export to Gif
    %       if n == 1;
    %           imwrite(imind,cm,filename,'gif', 'Loopcount',5);
    %       else
    %           imwrite(imind,cm,filename,'gif','WriteMode','append');
    %       end
    %    end
    
    tn = tnp;  % for next time step
    
    str = ['time = ',num2str(tnp)];
    disp(str);
    
    
    
end;
xlabel('x[m]','fontsize',10);ylabel('S_w[-]','fontsize',10);



po
lambda
fi
Ko
miw
mio
Swr;
Sor
u_an = u_an';
save u_an1.dat u_an -ascii 

%--------------------------------------------------------------------------
%FUNCTION - "getinitialcond"
%--------------------------------------------------------------------------

%This function calculate the coefficients to be used in the numerical flux
%difinition
function [c] = getcoefficient_gl(order,h)
%Define the matrix "c"

   %order = 3;


c = zeros(order+1,order);

%Calculate the coefficients to each face "j", which is from 0 until "k"
%Rows of "c"
for l = 1:order
    %Columns of "c"
    for j = 0:order
        denom = 0;
        innersum = 0;
        for r = l:order
            %Initialize inner counter "i" and "differ_rq"
            i = 0;
            differ_rq = 0;
            for q = 0:order
                if q ~= r
                    %Add "i"
                    i = i + 1;
                    %Calculate the difference inside denominator equation
                    %(see equation 3.23 Wang, 2002)
                    %t = partition(order,domain,order);
                    differ_rq(i) = (h(1,r+1) - h(1,q+1));
                    %differ_rq(i) = (r - q);
                end  %End of IF (denominator, r - q)
            end  %End of FOR (q)
            %Obtain the produtory
            denom(r) = prod(differ_rq);
            %Do the loop to internal somatory (m)
            %Initialize iner counter "i"
            im = 0;
            prod_jq = 0;
            for m = 0:order
                if m ~= r
                    %Increment "im"
                    im = im + 1;
                    %Do a loop inside somatory (produtory j - q)
                    %Initialize iner counter "i"
                    iq = 0;
                    differ_jq = 0;
                    for q = 0:order
                        if q ~= r && q ~= m
                            %Increment "iq"
                            iq = iq + 1;
                            %Calculate j - q
                            differ_jq(iq) = (h(1,j+1) - h(1,q+1));
                            %differ_jq(iq) = (j - q);
                        end  %End of IF (differ j - q)
                    end  %End of FOR (q)
                    prod_jq(im) = prod(differ_jq);
                end  %End of IF
            end  %End of FOR (m)
            %Calculate the internal somatory
            innersum(r) = (1/denom(r))*sum(prod_jq);
        end  %End of FOR (r)
        %Calculate the outer sum
        outsum = (h(l+1) - h(l))*sum(innersum);
        %outsum = sum(innersum);
        %Attribute the value calculated to matrix "c"
        %format rational
        c(j+1, l) = outsum;
    end  %End of FOR (j, columns)
end  %End of FOR (l, rows)



            
                
    
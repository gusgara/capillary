clc;
clear all

%% k=2, Buckley 1000s, L1
% x = [0.0364
%      0.0187
%      0.0102
%      0.0055
%      0.0031];
% delta_x = [0.03/32 0.03/64 0.03/128 0.03/256 0.03/512];
%% k=2, Buckley 10000s, L1
% x = [0.0447
%      0.0244
%      0.0133
%      0.0073
%      0.0040];
% delta_x = [0.03/32 0.03/64 0.03/128 0.03/256 0.03/512];
%% k=2, Buckley 1000s, L2
% x = [0.1245
%      0.0896
%      0.0650
%      0.0472
%      0.0348];
% delta_x = [0.03/32 0.03/64 0.03/128 0.03/256 0.03/512];
%% k=2, Buckley 10000s, L2
x = [0.1350
     0.1010
     0.0719
     0.0552
     0.0392];
delta_x = [0.03/32 0.03/64 0.03/128 0.03/256 0.03/512];
%% 
np = length(x); 
logaritmo_delta_x = zeros(1,np);
logaritmoL1 = zeros(1,np);
for i = 1:np
    logaritmo_delta_x(i) = log10(delta_x(i));
    logaritmoL1(i) = log10(x(i));
end
n = 1;
for j = 2: np
    k(n) = (log10(x(j)/x(j-1))/log10(delta_x(j)/delta_x(j-1)));
    n = n + 1;
end
disp(k')
load('-mat','10000L1.mat');
plot(logaritmo_delta_x,logaritmoL1,'r-s')
hold on
load('-mat','10000L2.mat');
plot(logaritmo_delta_x,logaritmoL1,'b-o')
% hold on
% load('-mat','bgk=4.mat');
% plot(logaritmo_delta_x,logaritmoL1,'m-h')
% load('-mat','bgk=5.mat');
% plot(logaritmo_delta_x,logaritmoL1,'g-<')
% load('-mat','bgk=6.mat');
%plot(logaritmo_delta_x,logaritmoL1,'k-o')
grid on
%axis([-2.51 -1 -11 -1])
axis square
xlabel('Log10(\Delta x)','fontsize',13)
ylabel('Log10(L Error)','fontsize',13)
legend('L1','L2')
title('Refinamento h','fontsize',13)
%%
%save('k=2.dat','logaritmo_delta_x','logaritmoL1','-ascii')
% savefile = '10000L2.mat';
% save(savefile, 'logaritmo_delta_x', 'logaritmoL1');
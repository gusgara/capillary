function[Rd] = EllipticDO(order,nsv,hcv,ucv,D)
%% Calculo do Fluxo difusivo nos contornos dos CV pelo FVM
global Sor Swr epsilon;
ncv = order*nsv;%Numero de celulas
if order == 1
satcell = [2*(0.999-Sor)-ucv(1),ucv,2*(0.001 + Swr)-ucv(length(ucv))];%Saturacao cell-center mas contornos 
dsat = (satcell(2:length(satcell))-satcell(1:length(satcell)-1))/hcv;%Derivada da saturacao nas interfaces numericas
um = [2*(0.999-Sor)-ucv(1),ucv];%Saturacoes a esquerda
up = [ucv,2*(0.001 + Swr)-ucv(length(ucv))];%Saturacoes a direita
else
satcell = [(0.999)-Sor,ucv,(0.001) + Swr];%Saturacao cell-center mas contornos
dsat = derivada(ncv,hcv,satcell)';%Derivada da saturacao nas interfaces numericas
um = [(0.9999)-Sor,ucv];%Saturacoes a esquerda
up = [ucv,(0.0001) + Swr];%Saturacoes a direita
end
%Alocando memoria
%Do = zeros(1,length(satcell)-1);
%satbound = zeros(1,length(satcell)-1);

Sw = 0.5*(um+up);%Media das saturacoes ao lado das interfaces numericas

    Do = D(Sw);%Coeficiente de difusao
    
    Rd = epsilon.*Do.*dsat;%Fluxo diffusivo nos contornos dos CV

end
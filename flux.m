function h = flux(Ff,Fdf,dfdu,ul,ur,strategy)
% Compute two points Lipschitz continuous monotone fluxes
% based on Cockburn & Shu (1989) Math of Comp. Vol.52, No 186 PP.411-435 
%% Compute the flux using 1 the following strategies:
switch strategy
    % assume a = u(i) and b = u(i+1)
    
    case {1} % Godunov
        
        
            % if a <= b choose min f(u) of (a<u<b) 
            % if a >= b choose max f(u) of (a>u>b)
            if ul <= ur
                h = min(Ff(ul),Ff(ur));
            else % u(i+1) <= u(i)
                h = max(Ff(ul),Ff(ur));
            end
        
        
    case {2} % Roe with entropy fix
        
        % Choose f(a) if f'(u) >= 0 for u that belongs [min(a,b),max(a,b)]
        % Choose f(b) if f'(u) <= 0 for u`that belongs [min(a,b),max(a,b)]
        % Otherwise choose LLF flux
        
            u_ave = (ul + ur)/2;
            if Fdf(u_ave) > 0 % if positive
                h = Ff(ul);
            elseif Fdf(u_ave) <= 0 % if negative
                h = Ff(ur);
            else
                h = 0;
            end
        
        
    case {3} % (Global) Lax Friedrichs
        
        % Compute  max(|f'(u)|) 
        % This max value is computed over the entire region of u, that is
        % [inf u(x), sup u(x)] where u(x) is the initial function.
        alpha = max(abs(dfdu));
        % Compute Lax-Friedrichs Flux
        
            h = 0.5*( Ff(ul) + Ff(ur) - alpha *(ur - ul));
        
        
    case {4} % Local Lax-Friedrichs
        
        % Similarly for Lax-Friedrichs:
        
            beta = max(abs(Fdf(ul)),abs(Fdf(ur)));
            h = 0.5*( Ff(ul) + Ff(ur) - beta *( ur - ul));
        
        
    case {5} % Simple Upwind
        
    % For dflux is constant along the domain!
    % if dflux > 0, flux to the left, then h(a,b) = h(a)
    % if dflux > 0, flux to the left, then h(a,b) = h(b)
    % We evaluate professor yang's strategy:
    % % a = (a - |a|)/2
    a = max(dfdu - abs(dfdu))/2; 
    
        if a == 0 % a > 0 
            h = Ff(ul); % Flux to the left
        else % a < 0 
            h = Ff(ur); % Flux to the right
        end
    
    
    otherwise
        error('flux strategy not available')
end
%--------------------------------------------------------------------------
%UNIVERSIDADE FEDERAL DE PERNAMBUCO
%CENTRO DE TECNOLOGIA E GEOCIENCIAS
%PROGRAMA DE POS GRADUACAO EM ENGENHARIA MEC�NICA
%TOPICOS ESPECIAIS EM DINAMICA DOS FLUIDOS COMPUTACIONAL
%--------------------------------------------------------------------------
%Subject: numerical routine to solve "Spectral Method" 1D
%Type of file: MAIN
%Criate date: 18/07/2012
%Modify data: 3/11/2014
%Programer: Gustavo Galindez Ramirez
%With contributions from Marcio Souza
%--------------------------------------------------------------------------
%Goals:
%Determinate a saturation field to 1D transient problem do flow two phase
%using "Spectral volume method".
%%
%Clear memory and scream
clc;
clear all;
%close all;
%Tipo de formato

%Number of Spectral volume
nsv = 256;%Cells

%Order of solution (in 1D this is equal to number of control volumes by
%each spectral volume)
order = 2;
%Courant number
CFL = 0.1;
%Total time
finaltime = 10000.0;
%Other parameters:

%Domain
domain = 0.03;
%Mesh data:
%Number of control volumes
ncv = nsv*order;
%"hsv" is the length of each spectral volume
hsv = domain/nsv;
%"hcv" is the length of each control volume inside each spectral volume
hcv = hsv/order;
%delta_t (dt)
%t-v ==> tempo variavel
%t-f ==> tempo fixo
dt = 't-f';
% Boundary wetting flux/total fluid flux
%gam = 0;%Unidirectinal displacement
%gam = 1;%Bidirectional displacement
gam = 'B_L';% Hiperbolic case
%% Model parameters two flow phase
%syms s %variaveis simbolicas
%Viscosity ratio (mio/miw)cp
mio = 1.0;%Viscosidade do oleo
miw = 1.0;%Viscosidade da agua
%mob = ['Mobility = ',num2str(M)];
%disp(mob)
fi = 0.2; %Porosity
Ko = 10^-10;
A = 3*10^-7;

%%
global Sor Swr epsilon
Sor = 0.0001;%Saturacao de oleo residual
Swr = 0.0001;%Saturacao de agua residual
epsilon = 0.01;% Coeficiente para o control da forca capilar

model = 1;

switch model
    case 1
        %%Modelo de Corey and Burdine (1953)
        lambda = 2;
        po = 5000;
        krw = @(s) ((s-Swr)./(1-Swr)).^(3+(2./lambda));%Relative permeability of the wetting  phase
        krn = @(s) (1-((s-Swr)./(1-Swr))).^2.*(1-((s-Swr)./(1-Swr)).^(1+(2./lambda)));%Relative permeability non-wetting phase
        PcD = @(s) po.*((s-Swr)./(1-Swr)).^(-1./lambda);
        dPcDdSw =  @(s) -po./(lambda.*((s-Swr)./(1-Swr)).^((1 + lambda)./lambda));
        f = @(s) (1 + (miw./mio).*((1-((s-Swr)./(1-Swr))).^2.*(1-((s-Swr)./(1-Swr)).^(1+(2./lambda))))./(((s-Swr)./(1-Swr)).^(3+(2./lambda)))).^-1;
        df = @(s) ((miw.*(2.*((s-Swr)./(1-Swr)) - 2).*(((s-Swr)./(1-Swr)).^(2./lambda + 1) - 1))./(mio.*((s-Swr)./(1-Swr)).^(2./lambda + 3)) - (miw.*(2./lambda + 3).*(((s-Swr)./(1-Swr)).^(2./lambda + 1) - 1).*(((s-Swr)./(1-Swr)) - 1).^2)./(mio.*((s-Swr)./(1-Swr)).^(2./lambda + 4)) + (miw.*((s-Swr)./(1-Swr)).^(2./lambda).*(2./lambda + 1).*(((s-Swr)./(1-Swr)) - 1).^2)./(mio.*((s-Swr)./(1-Swr)).^(2./lambda + 3)))./((miw.*(((s-Swr)./(1-Swr)).^(2./lambda + 1) - 1).*(((s-Swr)./(1-Swr)) - 1).^2)./(mio.*((s-Swr)./(1-Swr)).^(2./lambda + 3)) - 1).^2;
        D = @(s) -(Ko./mio).*(1-((s-Swr)./(1-Swr))).^2.*(1-((s-Swr)./(1-Swr)).^(1+(2./lambda)))...
            .*(1 + (miw./mio).*((1-((s-Swr)./(1-Swr))).^2.*(1-((s-Swr)./(1-Swr)).^(1+(2./lambda))))./(((s-Swr)./(1-Swr)).^(3+(2./lambda)))).^-1 ...
            .*  -po./(lambda.*(s).^((1 + lambda)./lambda));
    case 2
        %% Parker model
        m = 0.5;
        krw = @(s) sqrt((s-Swr)./(1-Swr-Sor)).*(1-(1-s.^(1./m)).^m).^2;
        krn = @(s) sqrt(1-((s-Swr)./(1-Swr-Sor))).*(1-((s-Swr)./(1-Swr-Sor)).^(1./m)).^2.*m;
        pcD = @(s) (((s-Swr)./(1-Swr-Sor)).^(-1./m)-1).^(1-m);
        dpcDdSw = @(s) pcD.*(1-m)./(m.*((s-Swr)./(1-Swr-Sor)).*(((s-Swr)./(1-Swr-Sor)).^(1./m)-1));
        f = @(s) (1 + (miw./mio).*((sqrt(1-((s-Swr)./(1-Swr-Sor))).*(1-((s-Swr)./(1-Swr-Sor)).^(1./m)).^2.*m)./(sqrt(((s-Swr)./(1-Swr-Sor))).*(1-(1-((s-Swr)./(1-Swr-Sor)).^(1./m)).^m).^2))).^(-1);
        df = @(s) ((m.*miw.*(s.^(1./m) - 1).^2)./(2.*mio.*s.^(1./2).*((1 - s.^(1./m)).^m - 1).^2.*(1 - s).^(1./2)) + (m.*miw.*(1 - s).^(1./2).*(s.^(1./m) - 1).^2)./(2.*mio.*s.^(3./2).*((1 - s.^(1./m)).^m - 1).^2) - (2.*miw.*s.^(1./m - 1).*(1 - s).^(1./2).*(s.^(1./m) - 1))./(mio.*s.^(1./2).*((1 - s.^(1./m)).^m - 1).^2) - (2.*m.*miw.*s.^(1./m - 1).*(1 - s).^(1./2).*(s.^(1./m) - 1).^2.*(1 - s.^(1./m)).^(m - 1))./(mio.*s.^(1./2).*((1 - s.^(1./m)).^m - 1).^3))./((m.*miw.*(1 - s).^(1./2).*(s.^(1./m) - 1).^2)./(mio.*s.^(1./2).*((1 - s.^(1./m)).^m - 1).^2) + 1).^2;
        
        D = @(s) -(1./mio).*(sqrt(1-s).*(1-s.^(1./m)).^2.*m)...
            .*( (1 + (miw./mio).*((sqrt(1-s).*(1-s.^(1./m)).^2.*m)./(sqrt(s).*(1-(1-s.^(1./m)).^m).^2))).^(-1))...
            .* ((s.^(-1./m)-1).^(1-m)).*(1-m)./(m.*s.*(s.^(1./m)-1));
end
sw = Swr:0.01:1-Sor;%vetor condicao inicial para avaliar a df/du
dfdu = df(sw);
Dmax = max(D(sw));

% Function g(t)
func_gt = 0;
switch func_gt
    case 0
        gt = @(t) 1;
    case 1
        gt = @(t) (1./(1+t));
    case 3
        gt = @(t) t.^(-1./2);
    case 4
        gt = @(t) exp(sin(t./100));
    case 5
        gt = @(t) (1+sin(t./10))./(1+abs(sin(t./10)));
end

%% Escolha da particao para delta_x na formula conservativa

h = chebyshev(order-1,0,hsv);% Distribution of partitions in computational domain

hc = h(2:order+1) - h(1:order);%size of partitions for G_L points
if order == 1
    c = [1;1];
else
    c = getcoefficient_gl(order,h);
end


%% Initial condition
[u,x] = getinitialcond_mod(nsv,hsv,order,domain,hc);
%% Calculo do delta_t
%Velocity(max)
amax = max(abs(dfdu));
switch dt
    %Calculate the number of timesteps and reconstruction coefficients
    %% Abordagem delta_t em funcao do CFL
    case 't-v'
        delta_t = timestp_mod(CFL,amax,hcv);
        nt = finaltime/delta_t;
        %% Abordagem delta_t fixo
    case 't-f'
        delta_t = 0.5;
        %CFL = (Dmax/fi)*(delta_t/(hcv^2));
        CFL = delta_t*((amax/hcv)+(Dmax/hcv^2));
        disp(CFL)
        if CFL > 1/2
            disp('WARNING CFL > 1/2')
            %             while CFL > 1/2
            %                 delta_t = input(sprintf('Please insert a smaller delta_t  to %3.3e :',delta_t));
            %                 CFL = (Dmax/fi)*(delta_t/(hcv^2));
            %             end
        end
end
NSTEP = round(finaltime / delta_t);% number of time steps
ploter= [1 NSTEP/4 NSTEP/2 3*NSTEP/4 NSTEP];
%% alocando memoria
R = zeros(1,ncv);
%% Start a stopwatch timer.
tic;
tn = 0;
for n=1:NSTEP
    
    
    
    %NUMERICAL SOLUTION
    tnp = tn + delta_t;   % = t_{n+1}
    
    qt = A.*gt(tnp); %Volumetric flux [m/s]
    
    
    %(Advance via conservative formula) Using Runge-Kutta ( order 3)
    u0= u;
    for istage= 1:3;
        switch gam
            
            case 0

                Rd = EllipticDO(order,nsv,hcv,u,D);%Elliptic Operator
                %%
                j = 1;
                for iu = 1:nsv
                    for i=1:order
                        
                        
                        R(j) =  (1./(fi.*hc(i).^2)).*(Rd(j+1) - Rd(j));
                        
                        j = j + 1;
                    end
                end  %End of FOR
                
            case 1
                Ri = HiperbolicDO(u,nsv,order,c,f,df,dfdu);%Hiperbolic Operator
                Rd = EllipticDO(order,nsv,hcv,u,D);%Elliptic Operator
                %%
                j = 1;
                for iu = 1:nsv
                    for i=1:order
                        
                        
                        R(j) = ...
                            - (qt./(fi.*hc(i))).*(Ri(j + 1) - Ri(j)) + (1./(fi.*hc(i).^2)).*(Rd(j+1) - Rd(j));
                        
                        j = j + 1;
                    end
                end  %End of FOR
                
            case 'B_L'
                Ri =  HiperbolicDO(u,nsv,order,c,f,df,dfdu);%Hiperbolic Operator
                %%
                j = 1;
                for iu = 1:nsv
                    for i=1:order
                        
                        
                        R(j) = ...
                            - (qt./(fi.*hc(i))).*(Ri(j + 1) - Ri(j));
                        
                        j = j + 1;
                    end
                end  %End of FOR
        end
        %% third-stage Runge-Kutta method
        if istage==1;
            u1= u0 + delta_t*R;
            u= u1;
        elseif istage==2;
            u2= (3/4)*u0 + (1/4)*u1 + (1/4)*delta_t*R;
            u= u2;
        else
            u= (1/3)*u0 + (2/3)*u2 + (2/3)*delta_t*R;
        end;
    end;
    
    
    if ismember(n,ploter)==1;
        figure(1);
        plot(x,u,'r','marker','o','markersize',4);
        %axis ([0 L -0.2 (1-Sor)]);
        hold on;grid on;drawnow
        
    end;
    
    %%
    
    tn = tnp;   % for next time step
    str = ['time = ',num2str(tnp)];
    disp(str)
    
end  %End of FOR
fprintf('Time of Simulation  = %1.1e',toc)
xlabel(' x[m]','fontsize',10);ylabel(' S_w[-]','fontsize',10);
%% ERROR
    u_an = load('-ascii','u_an1.dat');
    np = length(u);
    error = u' - u_an;    
    norm1 = norm(error,1)/np
    norm2 = norm(error,2)/sqrt(np)
    hcv
%

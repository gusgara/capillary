function[dsat]= derivada(N,h,u)


% N = 5;
% h = 0.2;
% x = (0.1:0.2:0.9)
% u = [1 x 0]'

D1 = spalloc(N+1,N+2,2*(N+1));
R = [-1 1 zeros(1,N-2)]/h;
C = [-1 zeros(1,N-2)]/h;
D1(2:N,2:N+1) = toeplitz(sparse(C),sparse(R));
D1(1,:) = [-1 1 zeros(1,N)]/(h/2);
D1(end,:) = [zeros(1,N) -1 1]/(h/2);

dsat = D1*u';
%dsat = real(dsat);

return
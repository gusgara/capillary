function[fL,fR] = DOFDM(u,m,f)

for i=1:m-1
    fR(i) = f(u(i+1));
    fL(i) = f(u(i));
end

end
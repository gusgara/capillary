function[Ri] = HiperbolicDO(ucv,nsv,order,c,f,df,dfdu)
%Hiperbolic Operator using Spectal Finite Volume Method
global Swr Sor
%% Criando a matriz de reconstrucao 'd'
strategy = 4;
d = zeros(nsv,order+1);
y = zeros(1,order);
n = 1;
for i=1:length(ucv)/order
    
    for j=1:order
        
        y(j) = ucv(n);
        
        n = n + 1;
    end
    
    b = (c*y')';
    
    for k=1:length(b)
        
        d(i,k) = b(k);
              
    end  
    for k=1:length(y)
        
        M(i,k) = y(k);
        
    end
end
%% CC CVTDVM limiter
if order == 1
    limit = 'L-off';
else
    limit = 'L-on';
end
ucvcc = [1-Sor,ucv,Swr];
%% Valores ul e ul reconstruidos a lado esq e dir resp da inteface entre SVs
UR = zeros(1,nsv-1);
UL = zeros(1,nsv-1);
%% Interfaces nos extremos do dominio computacional
switch limit
    case 'L-off'
        URR = ucvcc(2);
        ULL = ucvcc(1);
    case 'L-on'
        URR = ucvcc(2) - minmod(ucvcc(2)-d(1,1), ucvcc(3)-ucvcc(2),ucvcc(2)-ucvcc(1));%correc
        ULL = ucvcc(1) + minmod(ucvcc(1) - ucvcc(1),ucvcc(2)-ucvcc(1),ucvcc(1)-ucvcc(1));
end
%% Interfaces internas para os SVs no dominio computacional
h=1;
for m=2:length(ucv)/order
    switch limit
        case 'L-off'
            UL(h) = d(m-1,order + 1);
            UR(h) = d(m,1);
        case 'L-on'
            UL (h) = M(m-1,order) + minmod(d(m-1,order + 1) - M(m-1,order),M(m-1,order)-M(m-1,order-1),M(m,1)-M(m-1,order));
            UR(h) = M(m,1) - minmod(M(m,1)-d(m,1),M(m,1)-M(m-1,order), M(m,2)-M(m,1));%correc
    end
    h = h+1;
end
%% Calculo do fluxo nas interfaces numericas
Ri= zeros(1,(nsv*order)+1);
Ri(1) = flux(f,df,dfdu,ULL,URR,strategy);
Ri((nsv*order) + 1) = f(d(nsv,order + 1));
g = order + 1;
for j = 1: nsv - 1
    
    Ri(g) = flux(f,df,dfdu,UL(j),UR(j),strategy);%Flux entre SVs
    
    g = g+order;
end

p = 2;
for i = 1: nsv
    for j = 1: order - 1
        switch limit
            case 'L-off'
                Ri(p) = f(d(i,j+1));%Flux entre CVs
            case 'L-on'
                gradm = ucvcc(p) + minmod(d(i,j+1)-ucvcc(p),ucvcc(p)-ucvcc(p-1),ucvcc(p+1)-ucvcc(p));
                gradp = ucvcc(p+1) - minmod(ucvcc(p+1)-d(i,j+1),ucvcc(p+1)-ucvcc(p),ucvcc(p+2)-ucvcc(p+1));
                if gradm == gradp
                    Ri(p) = f(d(i,j+1));%Analitical Flux entre CVs
                else
                    Ri(p) = flux(f,df,dfdu,gradm,gradp,strategy);%Numerical Flux between CVs
                 %   cont = cont + 1
                    
                end
        end
        p = p + 1; %Interfaces Internas
    end  
    p = p + 1;%Salto interface numerica RSolver
end

    
    
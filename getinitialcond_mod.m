%--------------------------------------------------------------------------
%FUNCTION - "getinitialcond"
%--------------------------------------------------------------------------

%This function load the initial condition and the position of both control
%volume colocation point and numerical flux colocation.
function [u,x] = getinitialcond_mod(nsv,hsv,order,domain,hc)

%% Initialize poscv "cell-center" in order calculate the control volume center
        
        C = zeros(1,nsv*order);
        %Vetor B armazena o valor do tamanho das partiçoes
        
        B = hc;
        n=1;
        m=1;
        
        for k=1:length (B)
            if k == 1
                %if domain == 1
                    C(m) = B(k)/2;
%                 else
%                     C(m) = -1 + B(k)/2;
%                 end
                
            else
                %if domain == 1
                    C(m) = sum(B(1:k-1)) + B(k)/2;
%                 else
%                     C(m) =  -1 + (sum(B(1:k-1)) + B(k)/2);
%                 end
            end
            m=m+1;
        end
        x = C;
        for i=1:nsv-1
            for j=1:order
                
                x(n+order) = C(n) + hsv;
                n = n+1;
                
            end
            C = x;
            
        end
%% Define the position and value of initial condition in each control volume
poscv = zeros(1,nsv*order);
        
poscv = x;
%% Aplicacao da condicao inicial
% Sw(x,0) = Srw = 0 para 0 < x < 1
ucv = zeros(1,length(poscv));
global Swr;
for i=1:length(poscv)
    if 0 < poscv(i) && poscv(i) < domain
        
        u(i) = (0.0001) + Swr;
    end
end

end  %Fim do FOR



function[s] = minmod(x,y,z)

% x = -2;
% y = -3;
% z = -7;

sx = sign(x);
sy = sign(y);
sz = sign(z);

nx = abs(x);
ny = abs(y);
nz = abs(z);

a = [nx ny nz];

if  sx == sy &&  sy == sz
    
    s = sx*min(a);
    
    
else
    
    s = 0;
    
end
return